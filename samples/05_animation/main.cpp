#include <Saga.h>
#include <chrono>
#include <iostream>

using namespace saga;
using namespace core;
using namespace video;

struct CameraMatrix {
  glm::mat4 View;
  glm::mat4 Projection;
};

int main(int argc, char* argv[]) {
  auto device = createDevice(E_DRIVER_TYPE::VULKAN, {1280, 720}, 16, false, false, false);
  auto& driver = device->getVideoDriver();
  auto& smgr = device->getSceneManager();

  auto& cam = smgr->addCameraSceneNode();
  cam->setFarValue(200);

  auto mesh = smgr->getMesh("../media/wolf.x");
  auto texture = driver->createTexture("../media/wolf.jpg");

  auto cube = smgr->getMesh("../media/cube.obj");
  auto skyboxInfo = driver->createTexture();
  driver->loadTexture(skyboxInfo, 0, 0, "../media/desertsky_rt.tga");
  driver->loadTexture(skyboxInfo, 1, 0, "../media/desertsky_lf.tga");
  driver->loadTexture(skyboxInfo, 2, 0, "../media/desertsky_up.tga");
  driver->loadTexture(skyboxInfo, 3, 0, "../media/desertsky_dn.tga");
  driver->loadTexture(skyboxInfo, 4, 0, "../media/desertsky_bk.tga");
  driver->loadTexture(skyboxInfo, 5, 0, "../media/desertsky_ft.tga");
  skyboxInfo.Type = E_TEXTURE_TYPE::CUBE_MAP;
  auto skybox = driver->createTexture(std::move(skyboxInfo));
  auto sky = smgr->createSceneNode(cube);
  sky->setTexture(0, skybox);

  auto skyboxMatrixInfo = driver->createShaderUniform();
  skyboxMatrixInfo.Size = sizeof(CameraMatrix);
  auto skyboxMatrix = driver->createResource(std::move(skyboxMatrixInfo));

  {
    auto shaderInfo = driver->createShader();
    shaderInfo.VSSource = R"(
      #version 450
      layout (location = 0) in vec3 pos;

      layout (binding = 0) uniform SkyBoxMatrix
      {
        mat4 View;
        mat4 Projection;
      } sky;

      layout (location = 0) out vec3 uvw;

      void main() {
        uvw = pos;
        gl_Position = sky.Projection * sky.View * vec4(pos, 1.0);
      }
    )";

    shaderInfo.FSSource = R"(
      #version 450
      layout (location = 0) in vec3 uvw;
      layout (location = 0) out vec4 fragColor;
      layout (binding = 1) uniform samplerCube cubeMap;

      void main() {
        fragColor = texture(cubeMap, uvw);
      }
    )";

    auto pipelineInfo = driver->createPipeline();
    pipelineInfo.Shaders = driver->createResource(std::move(shaderInfo));
    pipelineInfo.Layout.Attributes[0][0] = {
      E_ATTRIBUTE_TYPE::POSITION,
      E_ATTRIBUTE_FORMAT::FLOAT3,
    };
    // skybox needs front face culling
    pipelineInfo.Rasterizer.CullMode = E_CULL_MODE::FRONT_FACE;
    pipelineInfo.Rasterizer.DepthClamp = true;

    auto pipeline = driver->createResource(std::move(pipelineInfo));
    sky->setPipeline(pipeline);
  }

  if (!mesh) return 1;
  if (!texture) return 1;
  auto node = smgr->createSceneNode(mesh);
  node->setTexture(0, texture);
  node->setScale({ 10.f, 10.f, 10.f });
  node->setRotation({ 90, 0, -130});
  node->setPosition({0, -1, -10});

  auto shaderInfo = driver->createShader();
  shaderInfo.VSSource = R"(
    #version 450
    layout (location = 0) in vec4 position;
    layout (location = 1) in vec2 texCoord;
    layout (location = 2) in vec4 boneWeights;
    layout (location = 3) in vec4 boneIDs;

    #define MAX_BONE 64

    layout (binding = 0) uniform CameraMatrix
    {
      mat4 View;
      mat4 Projection;
    } camera;

    layout (binding = 1) uniform ModelMatrix
    {
      mat4 Model;
    } model;

    layout (binding = 2) uniform Animation
    {
      mat4 Bones[MAX_BONE];
    } animation;

    layout (location = 0) out vec2 uv;

    void main() {
      uv = texCoord;
      mat4 boneTransform = animation.Bones[int(boneIDs.x)] * boneWeights[0];
      boneTransform     += animation.Bones[int(boneIDs.y)] * boneWeights[1];
      boneTransform     += animation.Bones[int(boneIDs.z)] * boneWeights[2];
      boneTransform     += animation.Bones[int(boneIDs.w)] * boneWeights[3];
      gl_Position = camera.Projection * camera.View * model.Model * boneTransform * position;
    }
  )";

  shaderInfo.FSSource = R"(
    #version 450

    layout (binding = 3) uniform sampler2D texture0;
    layout (location = 0) in vec2 uv;
    layout (location = 0) out vec4 fragColor;

    void main() {
      fragColor = texture(texture0, uv);
    }
  )";

  auto pipelineInfo = driver->createPipeline();
  pipelineInfo.Shaders = driver->createResource(std::move(shaderInfo));

  // this model needs front face culling
  pipelineInfo.Rasterizer.CullMode = E_CULL_MODE::FRONT_FACE;

  pipelineInfo.Layout.Attributes[0][0] = {
    E_ATTRIBUTE_TYPE::POSITION,
    E_ATTRIBUTE_FORMAT::FLOAT3,
  };
  pipelineInfo.Layout.Attributes[0][1] = {
    E_ATTRIBUTE_TYPE::TEXTURE_COORDINATE,
    E_ATTRIBUTE_FORMAT::FLOAT2,
  };

  pipelineInfo.Layout.Attributes[0][2] = {
    E_ATTRIBUTE_TYPE::BONE_WEIGHT,
    E_ATTRIBUTE_FORMAT::FLOAT4,
  };

  pipelineInfo.Layout.Attributes[0][3] = {
    E_ATTRIBUTE_TYPE::BONE_ID,
    E_ATTRIBUTE_FORMAT::FLOAT4,
  };

  auto cameraUniformInfo = driver->createShaderUniform();
  cameraUniformInfo.Size = sizeof(CameraMatrix);
  auto cameraUniform = driver->createResource(std::move(cameraUniformInfo));

  auto modelUniformInfo = driver->createShaderUniform();
  modelUniformInfo.Size = sizeof(glm::mat4);
  auto modelUniform = driver->createResource(std::move(modelUniformInfo));

  auto animUniformInfo = driver->createShaderUniform();
  animUniformInfo.Size = sizeof(glm::mat4) * scene::MAX_BONES;
  auto animUniform = driver->createResource(std::move(animUniformInfo));

  auto passInfo = driver->createRenderPass();
  auto pipeline = driver->createResource(std::move(pipelineInfo));
  node->setPipeline(pipeline);

  auto pass = driver->createResource(std::move(passInfo));
  smgr->registerNode(node, pass);

  std::array<glm::mat4, scene::MAX_BONES> animation;
  node->setOnRender([mesh, node, cam, driver, modelUniform, animUniform, cameraUniform, &animation]() {
    CameraMatrix camMatrix;
    camMatrix.View = cam->getViewMatrix();
    camMatrix.Projection = cam->getProjectionMatrix();
    driver->updateShaderUniform(cameraUniform, &camMatrix);
    driver->bindShaderUniform(cameraUniform, 0);

    auto matrix = node->getAbsoluteTransformation();
    driver->updateShaderUniform(modelUniform, &matrix);
    driver->bindShaderUniform(modelUniform, 1);
    int i = 0;
    auto skinnedMesh = std::dynamic_pointer_cast<scene::ISkinnedMesh>(mesh);
    for (auto& mat : animation)
    {
      if (i >= skinnedMesh->getBoneCount())
        break;
      mat = skinnedMesh->getBoneTransform(i++);
    }
    driver->updateShaderUniform(animUniform, &animation);
    driver->bindShaderUniform(animUniform, 2);
    driver->bindTexture(node->getTexture(0), 3);
  });

  smgr->registerNode(sky, pass);
  sky->setOnRender([sky, cam, driver, skyboxMatrix]() {
    CameraMatrix matrix;
    matrix.View = glm::mat3(cam->getViewMatrix());
    matrix.View = cam->getViewMatrix();
    matrix.Projection = glm::perspective(glm::radians(60.0f), (float)driver->getWidth() / driver->getHeight(), 0.001f, 256.0f);
    driver->updateShaderUniform(skyboxMatrix, &matrix);
    driver->bindShaderUniform(skyboxMatrix, 0);
    driver->bindTexture(sky->getTexture(0), 1);
  });

  auto Then = std::chrono::high_resolution_clock::now();
  while (device->run()) {
    if (device->isWindowActive()) {
      smgr->animate();
      driver->begin();
      driver->beginPass(pass);

      driver->draw();
      driver->endPass();
      driver->end();
      driver->submit();
      driver->present();
    } else
      device->yield();

    auto Now = std::chrono::high_resolution_clock::now();
    auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(Now - Then).count();
    Then = Now;
    if (millis > 0)
    {
      auto sec = millis / 1000.f;
      int fps = 60.f / sec;
      device->setWindowCaption("Saga 3D GPU Skinning - FPS: " + std::to_string(fps));
    }
  }

  return 0;
}

