// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_SCENE_NODE_ANIMATOR_FOLLOW_SPLINE_H_INCLUDED__
#define __C_SCENE_NODE_ANIMATOR_FOLLOW_SPLINE_H_INCLUDED__

#include "ISceneNode.h"

#include "ISceneNodeAnimatorFinishing.h"

namespace saga
{
namespace scene
{
  //! Scene node animator based free code Matthias Gall wrote and sent in. (Most of
  //! this code is written by him, I only modified bits.)
  class CSceneNodeAnimatorFollowSpline : public ISceneNodeAnimatorFinishing
  {
  public:

    //! constructor
    CSceneNodeAnimatorFollowSpline(std::uint32_t startTime,
      const std::vector< glm::vec3 >& points,
      float speed = 1.0f, float tightness = 0.5f, bool loop=true, bool pingpong=false);

    //! animates a scene node
    virtual void animateNode(ISceneNode* node, std::uint32_t timeMs) override;

    //! Writes attributes of the scene node animator.
    virtual void serializeAttributes(io::IAttributes* out, io::SAttributeReadWriteOptions* options= 0) const override;

    //! Reads attributes of the scene node animator.
    virtual void deserializeAttributes(io::IAttributes* in, io::SAttributeReadWriteOptions* options= 0) override;

    //! Returns type of the scene node animator
    virtual ESCENE_NODE_ANIMATOR_TYPE getType() const override { return ESNAT_FOLLOW_SPLINE; }

    //! Creates a clone of this animator.
    /** Please note that you will have to drop
    (IReferenceCounted::drop()) the returned pointer after calling
    this. */
    virtual ISceneNodeAnimator* createClone(ISceneNode* node, ISceneManager* newManager= 0) override;

  protected:

    //! clamps a the value idx to fit into range 0..size-1
    std::int32_t clamp(std::int32_t idx, std::int32_t size);

    std::vector< glm::vec3 > Points;
    float Speed;
    float Tightness;
    bool Loop;
    bool PingPong;
  };


} // namespace scene
} // namespace saga

#endif

