// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CDefaultSceneNodeFactory.h"
#include "ISceneManager.h"
#include "ITextSceneNode.h"
#include "IBillboardTextSceneNode.h"
#include "ITerrainSceneNode.h"
#include "IDummyTransformationSceneNode.h"
#include "ICameraSceneNode.h"
#include "IBillboardSceneNode.h"
#include "IAnimatedMeshSceneNode.h"
#include "IParticleSystemSceneNode.h"
#include "ILightSceneNode.h"
#include "IMeshSceneNode.h"
#include "IOctreeSceneNode.h"

namespace saga
{
namespace scene
{


CDefaultSceneNodeFactory::CDefaultSceneNodeFactory(ISceneManager* mgr)
: Manager(mgr)
{

  #ifdef _DEBUG
  setDebugName("CDefaultSceneNodeFactory");
  #endif

  // don't grab the scene manager here to prevent cyclic references

  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::CUBE, "cube"));
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::SPHERE, "sphere"));
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::TEXT, "text"));
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::BILLBOARD_TEXT, "billboardText"));
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::WATER_SURFACE, "waterSurface"));
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::TERRAIN, "terrain"));
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::SKY_BOX, "skyBox"));
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::SKY_DOME, "skyDome"));
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::SHADOW_VOLUME, "shadowVolume"));
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::OCTREE, "octree"));
  // Legacy support
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::OCTREE, "octTree"));
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::MESH, "mesh"));
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::LIGHT, "light"));
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::EMPTY, "empty"));
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::DUMMY_TRANSFORMATION, "dummyTransformation"));
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::CAMERA, "camera"));
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::BILLBOARD, "billBoard"));
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::ANIMATED_MESH, "animatedMesh"));
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::PARTICLE_SYSTEM, "particleSystem"));
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::VOLUME_LIGHT, "volumeLight"));
  // SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::MD3_SCENE_NODE, "md3"));

  // legacy, for version <= 1.4.x irr files
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::CAMERA_MAYA, "cameraMaya"));
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::CAMERA_FPS, "cameraFPS"));
  SupportedSceneNodeTypes.push_back(SSceneNodeTypePair(E_SCENE_NODE_TYPE::Q3SHADER_SCENE_NODE, "quake3Shader"));
}


//! adds a scene node to the scene graph based on its type id
ISceneNode* CDefaultSceneNodeFactory::addSceneNode(E_SCENE_NODE_TYPE type, ISceneNode* parent)
{
  switch(type)
  {
  case E_SCENE_NODE_TYPE::CUBE:
    return Manager->addCubeSceneNode(10, parent);
  case E_SCENE_NODE_TYPE::SPHERE:
    return Manager->addSphereSceneNode(5, 16, parent);
  case E_SCENE_NODE_TYPE::TEXT:
    return Manager->addTextSceneNode(0, L"example");
  case E_SCENE_NODE_TYPE::BILLBOARD_TEXT:
    return Manager->addBillboardTextSceneNode(0, L"example");
  case E_SCENE_NODE_TYPE::WATER_SURFACE:
    return Manager->addWaterSurfaceSceneNode(0, 2.0f, 300.0f, 10.0f, parent);
  case E_SCENE_NODE_TYPE::TERRAIN:
    return Manager->addTerrainSceneNode("", parent, -1,
              glm::vec3(0.0f,0.0f,0.0f),
              glm::vec3(0.0f,0.0f,0.0f),
              glm::vec3(1.0f,1.0f,1.0f),
              video::SColor(255,255,255,255),
              4, E_TERRAIN_PATCH_SIZE::SIZE_17, 0, true);
  case E_SCENE_NODE_TYPE::SKY_BOX:
    return Manager->addSkyBoxSceneNode(0,0,0,0,0,0, parent);
  case E_SCENE_NODE_TYPE::SKY_DOME:
    return Manager->addSkyDomeSceneNode(0, 16, 8, 0.9f, 2.0f, 1000.0f, parent);
  case E_SCENE_NODE_TYPE::SHADOW_VOLUME:
    return 0;
  case E_SCENE_NODE_TYPE::OCTREE:
    return Manager->addOctreeSceneNode((IMesh*)0, parent, -1, 128, true);
  case E_SCENE_NODE_TYPE::MESH:
    return Manager->addMeshSceneNode(0, parent, -1, glm::vec3(),
                     glm::vec3(), glm::vec3(1,1,1), true);
  case E_SCENE_NODE_TYPE::LIGHT:
    return Manager->addLightSceneNode(parent);
  case E_SCENE_NODE_TYPE::EMPTY:
    return Manager->addEmptySceneNode(parent);
  case E_SCENE_NODE_TYPE::DUMMY_TRANSFORMATION:
    return Manager->addDummyTransformationSceneNode(parent);
  case E_SCENE_NODE_TYPE::CAMERA:
    return Manager->addCameraSceneNode(parent);
  case E_SCENE_NODE_TYPE::CAMERA_MAYA:
    return Manager->addCameraSceneNodeMaya(parent);
  case E_SCENE_NODE_TYPE::CAMERA_FPS:
    return Manager->addCameraSceneNodeFPS(parent);
  case E_SCENE_NODE_TYPE::BILLBOARD:
    return Manager->addBillboardSceneNode(parent);
  case E_SCENE_NODE_TYPE::ANIMATED_MESH:
    return Manager->addAnimatedMeshSceneNode(0, parent, -1, glm::vec3(),
                         glm::vec3(), glm::vec3(1,1,1), true);
  case E_SCENE_NODE_TYPE::PARTICLE_SYSTEM:
    return Manager->addParticleSystemSceneNode(true, parent);
  case E_SCENE_NODE_TYPE::VOLUME_LIGHT:
    return (ISceneNode*)Manager->addVolumeLightSceneNode(parent);
  default:
    break;
  }

  return 0;
}


//! adds a scene node to the scene graph based on its type name
ISceneNode* CDefaultSceneNodeFactory::addSceneNode(const char* typeName, ISceneNode* parent)
{
  return addSceneNode(getTypeFromName(typeName), parent);
}


//! returns amount of scene node types this factory is able to create
std::uint32_t CDefaultSceneNodeFactory::getCreatableSceneNodeTypeCount() const
{
  return SupportedSceneNodeTypes.size();
}


//! returns type of a creatable scene node type
E_SCENE_NODE_TYPE CDefaultSceneNodeFactory::getCreateableSceneNodeType(std::uint32_t idx) const
{
  if (idx<SupportedSceneNodeTypes.size())
    return SupportedSceneNodeTypes[idx].Type;
  else
    return E_SCENE_NODE_TYPE::UNKNOWN;
}


//! returns type name of a creatable scene node type
const char* CDefaultSceneNodeFactory::getCreateableSceneNodeTypeName(std::uint32_t idx) const
{
  if (idx<SupportedSceneNodeTypes.size())
    return SupportedSceneNodeTypes[idx].TypeName.c_str();
  else
    return 0;
}


//! returns type name of a creatable scene node type
const char* CDefaultSceneNodeFactory::getCreateableSceneNodeTypeName(E_SCENE_NODE_TYPE type) const
{
  for (std::uint32_t i = 0; i < SupportedSceneNodeTypes.size(); ++i)
    if (SupportedSceneNodeTypes[i].Type == type)
      return SupportedSceneNodeTypes[i].TypeName.c_str();

  return 0;
}


E_SCENE_NODE_TYPE CDefaultSceneNodeFactory::getTypeFromName(const char* name) const
{
  for (std::uint32_t i = 0; i < SupportedSceneNodeTypes.size(); ++i)
    if (SupportedSceneNodeTypes[i].TypeName == name)
      return SupportedSceneNodeTypes[i].Type;

  return E_SCENE_NODE_TYPE::UNKNOWN;
}


} // namespace scene
} // namespace saga

