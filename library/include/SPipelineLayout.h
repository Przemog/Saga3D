#ifndef __SPIPELINE_LAYOUT_H_INCLUDED__
#define __SPIPELINE_LAYOUT_H_INCLUDED__

#include "SGPUResource.h"
#include "GraphicsConstants.h"
#include "EAttributeTypes.h"
#include "EAttributeFormats.h"
#include <array>

namespace saga
{
namespace video
{
  struct SVertexAttribute
  {
    SVertexAttribute() : Type(E_ATTRIBUTE_TYPE::INVALID) {}
    SVertexAttribute(E_ATTRIBUTE_TYPE type, E_ATTRIBUTE_FORMAT format)
      : Type(type), Format(format) {}

    SVertexAttribute& operator=(const SVertexAttribute& other) = default;

    E_ATTRIBUTE_TYPE Type;
    E_ATTRIBUTE_FORMAT Format;
  };

  struct SPipelineLayout : public SGPUResource
  {
    SPipelineLayout()
    {

    }
    std::array<std::array<SVertexAttribute, MAX_VERTEX_ATTRIBUTES>, MAX_VERTEX_BUFFERS> Attributes;
  };

} // namespace scene
} // namespace saga

#endif // __SPIPELINE_LAYOUT_H_INCLUDED__
