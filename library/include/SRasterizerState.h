#ifndef __SRASTERIZER_STATE_H_INCLUDED__
#define __SRASTERIZER_STATE_H_INCLUDED__

#include "ECullMode.h"
#include "EFrontFaceMode.h"

namespace saga
{
namespace video
{
  struct SRasterizerState
  {
    E_CULL_MODE CullMode;
    E_FRONT_FACE_MODE FrontFaceMode;
    bool DepthClamp = false;
    int SampleCount;
  };

} // namespace scene
} // namespace saga

#endif // __SRASTERIZER_STATE_H_INCLUDED__

