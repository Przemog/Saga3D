#ifndef __GRAPHICS_CONSTANTS_H_INCLUDED__
#define __GRAPHICS_CONSTANTS_H_INCLUDED__

namespace saga
{
namespace video
{

  constexpr auto CUBE_FACE_COUNT = 6;
  constexpr auto MAX_MIPMAPS = 16;
  constexpr auto MAX_NODE_TEXTURES = 16;
  constexpr auto MAX_COLOR_ATTACHMENTS = 4;
  constexpr auto MAX_VERTEX_BUFFERS = 4;
  constexpr auto MAX_VERTEX_ATTRIBUTES = 4;

} // namespace video

namespace scene
{

  constexpr auto TRANSFORM_FRUSTUM_COUNT = 2;
  constexpr auto VIEW_FRUSTUM_PLANE_COUNT = 6;
  constexpr auto MAX_BONES = 64;
  constexpr auto MAX_BONES_PER_VERTEX = 4;

} // namespace scene

} // namespace saga

#endif

