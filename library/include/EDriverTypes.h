#ifndef __E_DRIVER_TYPES_H_INCLUDED__
#define __E_DRIVER_TYPES_H_INCLUDED__

namespace saga
{
namespace video
{
  //! An enum class for all types of drivers the Irrlicht Engine supports.
  enum class E_DRIVER_TYPE
  {
    //! Vulkan headless driver, useful for applications to run the engine without creating application window.
    /** This driver is able to render to images or compute. */
    VULKAN_HEADLESS,

    //! Vulkan device, available on most platforms.
    /** Performs hardware accelerated rendering of 3D and 2D primitives. */
    VULKAN,

    //! No driver, just for counting the elements
    COUNT
  };

  const char* const DRIVER_TYPE_NAMES[] =
  {
    "VulkanHeadlessDriver",
    "VulkanDriver",
    nullptr
  };

  const char* const DRIVER_TYPE_NAMES_SHORT[] =
  {
    "vkhl",
    "vk",
    nullptr
  };

} // namespace video
} // namespace saga

#endif
