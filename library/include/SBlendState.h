#ifndef __SBLEND_STATE_H_INCLUDED__
#define __SBLEND_STATE_H_INCLUDED__

#include "EBlendFactor.h"
#include "EPixelFormat.h"

namespace saga
{
namespace video
{
  struct SBlendState
  {
    int ColorAttachmentCount;
    E_PIXEL_FORMAT DepthFormat;
  };

} // namespace scene
} // namespace saga

#endif // __SBLEND_STATE_H_INCLUDED__

